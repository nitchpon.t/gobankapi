package model

// BankAccount - a struct contain all account information
type BankAccount struct {
	ID            int64  `json:"id"`
	UserID        int64  `json:"user_id"`
	AccountNumber string `json:"account_no"`
	Name          string `json:"name"`
	Balance       int64  `json:"balance"`
}

// Request - a struct contain all information required for a request
type Request struct {
	FromAccount int64 `json:"from_account"`
	ToAccount   int64 `json:"to_account"`
	Amount      int64 `json:"amount"`
}
