package dao

import (
	"bank/model"
)

// UserDao - provide user related data and service
type UserDao interface {
	ListAll() ([]model.User, error)
	Get(int64) (*model.User, error)
	Create(*model.User) error
	Update(*model.User) error
	Delete(int64) error

	MaxLength_FirstName() int
	MaxLength_LastName() int
}

// BankAccountDao - provide BankAccount related data and service
type BankAccountDao interface {
	Get(int64) (model.BankAccount, error)
	Create(*model.BankAccount) error
	Update(*model.Request) error
	Delete(int64) error

	MaxLength_AccountNumber() int
	MaxLength_AccountName() int
}
