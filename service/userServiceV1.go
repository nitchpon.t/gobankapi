package service

import (
	"bank/dao"
	"bank/model"

	"errors"
	"fmt"
)

// UserServiceV1 - first version fo user service need a UserDao to implement
type UserServiceV1 struct {
	UserDataService dao.UserDao
}

// ListAll - implement UserService function to list all user
func (service *UserServiceV1)ListAll() ([]model.User, error) {
	return service.UserDataService.ListAll()
}

// Get - implement UserService function to get a user
func (service *UserServiceV1)Get(id int64) (*model.User, error) {
	return service.UserDataService.Get(id)
}

// Create - implement UserService function to create a user; validate data before calling DAO
func (service *UserServiceV1)Create(userInfo *model.User) (*model.User, error) {
	if userInfo.ID != 0 {
		return nil, errors.New("Cannot specify ID when Creating a new User")
	}

	if "" == userInfo.FirstName {
		return nil, errors.New("FirstName Cannot be Nil when Creating a new User")
	}

	if "" == userInfo.LastName {
		return nil, errors.New("LastName Cannot be Nil when Creating a new User")
	}

	if service.UserDataService.MaxLength_FirstName() < len(userInfo.FirstName) {
		return nil, fmt.Errorf("\"%s\" is too long for a FirstName", userInfo.FirstName)
	}

	if service.UserDataService.MaxLength_LastName() < len(userInfo.LastName) {
		return nil, fmt.Errorf("\"%s\" is too long for a LastName", userInfo.LastName)
	}

	err := service.UserDataService.Create(userInfo)
	if (err != nil) {
		return nil, err
	}

	return userInfo, nil
}

// Update - implement UserService function to update a user information
func (service *UserServiceV1)Update(userInfo *model.User) (*model.User, error) {
	if userInfo.ID == 0 {
		return nil, errors.New("ID 0 is Reserved for Creating a new User")
	}

	if service.UserDataService.MaxLength_FirstName() < len(userInfo.FirstName) {
		return nil, fmt.Errorf("\"%s\" is too long for a FirstName", userInfo.FirstName)
	}

	if service.UserDataService.MaxLength_LastName() < len(userInfo.LastName) {
		return nil, fmt.Errorf("\"%s\" is too long for a LastName", userInfo.LastName)
	}

	err := service.UserDataService.Update(userInfo)
	if err != nil {
		return nil, err
	}

	after, err := service.UserDataService.Get(userInfo.ID)
	if err != nil {
		return nil, err
	}

	return after, nil
}

// Delete = implement UserService function to delete a user information
// related BankAccount deletion in implemented elsewhere
func (service *UserServiceV1)Delete(id int64) error {
	return service.UserDataService.Delete(id)
}