package service

import (
	"bank/dao"
	"bank/model"

	"testing"
)


type dummyDao struct {
	dao.UserDao
}

func (d *dummyDao) ListAll() ([]model.User, error) {
	return []model.User{}, nil
}
// var dummyDao = dao.UserDao

// var testService = UserServiceV1{
// 	UserDataService: dummyDao,
// }

func Test_Error_Create_WithID(t *testing.T) {
	testService := UserServiceV1{
	UserDataService: &dummyDao{},
	}
	user := &model.User {
		ID: 111,
		FirstName: "FirstName",
		LastName: "LastName",
	}
	_, err := testService.Create(user)
	if err == nil {
		t.Errorf("Create Should Error when ID is specified")
	}
}
