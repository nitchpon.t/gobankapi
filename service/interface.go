package service

import (
	"bank/model"
)

// UserService - User Related Service
type UserService interface {
	ListAll() ([]model.User, error)
	Get(int64) (*model.User, error)
	Create(*model.User) (*model.User, error)
	Update(*model.User) (*model.User, error)
	Delete(int64) error
}

// AccountService - Account Related Service
type AccountService interface{}
